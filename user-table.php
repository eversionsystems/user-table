<?php
/*
Plugin Name: User Table
Plugin URI: https://eversionsystems.com
Description: Output a list of WordPress users via a shortcode [show-user-table]. User table is filterable via user role and sortable.
Author: Andrew Schultz
Version: 1.0
Author URI: https://eversionsystems.com
Text Domain: user-table
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
};

class User_Table
{
    protected $wp_users_per_page = 10;

    const SORT_ORDER = 'sort_order';
    const SORT_COLUMN = 'sort_column';
    const WP_ROLE = 'wp_role';
    const PAGE_NUM = 'page_num';

    public function __construct()
    {
        $this->define_public_hooks();
    }

    private function define_public_hooks()
    {
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));

        //AJAX functions
        add_action('wp_ajax_click_pagination', array($this, 'click_pagination_handler'));
        add_action('wp_ajax_click_sort_column', array($this, 'click_sort_column_handler'));
        add_action('wp_ajax_click_filter_roles', array($this, 'click_filter_roles_handler'));

        //Shortcode for front end
        add_shortcode('show-user-table', array($this, 'output_user_table_func'));
    }

    public function enqueue_scripts()
    {
        $args = array(
            'number' => $this->wp_users_per_page
        );

        $wp_user_query = new WP_User_Query($args);
        $wp_users = $wp_user_query->get_results();
        $total_pages = ceil($wp_user_query->total_users / $this->wp_users_per_page);

        $args = array(
            'ajax_url' => admin_url('admin-ajax.php'),
            'total_pages' => $total_pages
            //'nonce' => wp_create_nonce('update-user-table'),
        );

        wp_enqueue_script('user-table', plugin_dir_url(__FILE__) . 'js/user-table.js', array('jquery'), filemtime(plugin_dir_path(__FILE__) . 'js/user-table.js'), true);
        wp_localize_script('user-table', 'ut', $args);

        wp_enqueue_style('dashicons');
    }

    /**
     * Generate user table HTML and pagination links for user role change event.
     *
     * @return void
     */
    public function click_filter_roles_handler()
    {
        $wp_role = $_POST[self::WP_ROLE];

        $sort_column = '';
        $sort_order = 'ASC';

        $args = array(
            'number' => $this->wp_users_per_page,
            'offset' => 0,
            'role' => $wp_role
        );

        $wp_user_query = new WP_User_Query($args);
        $wp_users = $wp_user_query->get_results();
        $total_pages = ceil($wp_user_query->total_users / $this->wp_users_per_page);

        ob_start();
        self::get_user_rows_html($wp_users);
        $table_data = ob_get_clean();

        $pagination_links = paginate_links(
            array(
                'base' => home_url() . '/?' . self::PAGE_NUM . '=%#%',
                //'format' => '?page_num=%#%&role=' . $wp_role,
                'total' => $total_pages,
                'prev_next' => false,
            )
        );

        wp_send_json(
            array(
                'pagination' => $pagination_links,
                'user_rows' => $table_data
            )
        );
    }

    /**
     * Generate user table HTML and pagination links for column sort click event.
     *
     * @return void
     */
    public function click_sort_column_handler()
    {
        $wp_role = $_POST[self::WP_ROLE];
        $sort_column = $_POST[self::SORT_COLUMN];
        $sort_order = $_POST[self::SORT_ORDER];

        $args = array(
            'number' => $this->wp_users_per_page,
            'offset' => 0,
            'orderby' => $sort_column,
            'role' => $wp_role,
            'order' => $sort_order
        );

        $wp_user_query = new WP_User_Query($args);
        $wp_users = $wp_user_query->get_results();
        $total_pages = ceil($wp_user_query->total_users / $this->wp_users_per_page);

        ob_start();
        self::get_user_rows_html($wp_users);
        $table_data = ob_get_clean();

        $pagination_links = paginate_links(
            array(
                'base' => home_url() . '/?' . self::PAGE_NUM . '=%#%',
                'total' => $total_pages,
                'current' => 1,
                'prev_next' => false,
            )
        );

        wp_send_json(
            array(
                'pagination' => $pagination_links,
                'user_rows' => $table_data
            )
        );

    }

    /**
     * Generate user table HTML and pagination links for clicked pagination link
     *
     * @return void
     */
    public function click_pagination_handler()
    {
        $pagination_url = $_POST['pagination_url'];
        $sort_order = $_POST[self::SORT_ORDER];
        $sort_column = $_POST[self::SORT_COLUMN];
        $wp_role = $_POST[self::WP_ROLE];

        $query_parts = self::extract_query_args($pagination_url);
        $page_num = $query_parts[self::PAGE_NUM];
        $user_offset = ($page_num - 1) * $this->wp_users_per_page;

        $args = array(
            'number' => $this->wp_users_per_page,
            'offset' => $user_offset,
            'role' => $wp_role,
            'orderby' => $sort_column,
            'order' => $sort_order
        );

        $wp_user_query = new WP_User_Query($args);
        $wp_users = $wp_user_query->get_results();
        $total_pages = ceil($wp_user_query->total_users / $this->wp_users_per_page);

        ob_start();
        self::get_user_rows_html($wp_users);
        $table_data = ob_get_clean();

        //Issue with page 1 missing pagination structure, can't use the 'format' parameter
        //https://wordpress.stackexchange.com/questions/87433/strange-paginate-links-behavior-first-page-link-is-always-whatever-page-im-on
        $pagination_links = paginate_links(
            array(
                'base' => home_url() . '/?' . self::PAGE_NUM . '=%#%',
                //'format' => '?page_num=%#%&role=' . $wp_role,
                'total' => $total_pages,
                'current' => $page_num,
                'prev_next' => false,
            )
        );

        wp_send_json(
            array(
                'pagination' => $pagination_links,
                'user_rows' => $table_data
            )
        );
    }

    /**
     * Extract the page number for pagination from a URL
     *
     * @param [string] $url
     * @return array
     */
    private function extract_query_args($url)
    {
        $query_args = array(
            self::PAGE_NUM => 1
        );

        $page_link_parts = parse_url($url);

        if (isset($page_link_parts['query'])) {
            parse_str($page_link_parts['query'], $query_parts);

            if (isset($query_parts[self::PAGE_NUM])) {
                $query_args[self::PAGE_NUM] = intval($query_parts[self::PAGE_NUM]);
            }
        }

        return $query_args;
    }


    /**
     * Shortcode function to return user table HTML
     *
     * @return string
     */
    public function output_user_table_func()
    {
        global $wp_roles, $post;

        //If user is not logged in then exit
        if (!is_user_logged_in()) {
            return 'Please <a href="' . wp_login_url(get_permalink()) . '">login</a> to view the user data.';
        }

        $current_user = wp_get_current_user();
        $allowed_roles = array('administrator');

        if (array_intersect($allowed_roles, $current_user->roles)) {
            $args = array(
                'number' => $this->wp_users_per_page,
                'offset' => 0
            );

            $wp_user_query = new WP_User_Query($args);
            $wp_users = $wp_user_query->get_results();
            $total_pages = ceil($wp_user_query->total_users / $this->wp_users_per_page);

            //Store HTML to output at end of shortcode function
            ob_start();

            ?>
            <select id="wp_role_select">
                <option value="">Select A Role</option>
                <?php foreach ($wp_roles->roles as $key => $value) : ?>
                <option value="<?= esc_html($key) ?>"><?= esc_html($value['name']) ?></option>
                <?php endforeach; ?>
            </select>

            <table id="user_table">
                <thead>
                    <tr>
                    <th><a id="th_user_name" href="#" class="sort-column">Username</a></th>
                    <th><a id="th_display_name" href="#" class="sort-column">Name</a></th>
                    <th>Role</th>
                    </tr>
                </thead>
                <tbody id="user_table_body">
            <?php

            self::get_user_rows_html($wp_users);

            ?>
                 </tbody>
            </table>
            <div id="pagination">
            <?php

            echo paginate_links(
                array(
                    'base' => home_url() . '/?' . self::PAGE_NUM . '=%#%',
                    //'format' => '?page_num=%#%',
                    'total' => $total_pages,
                    'prev_next' => false
                )
            );
            ?>
            </div>
            <div id="ajax_spinner" style="display: none;text-align: center;">
                <img src="<?= esc_url(includes_url() . 'images/wpspin.gif') ?>">
            </div>
            <?php

            $table_data = ob_get_clean();

        } else {
            $table_data = 'Only admins can view this data.';
        }

        return $table_data;
    }

    /**
     * Generate HTML for table rows from user objects
     *
     * @param [WP_User] $wp_users
     * @return void
     */
    private function get_user_rows_html($wp_users)
    {
        if (!$wp_users) {
            echo 'No users exist for that role.';
        }

        foreach ($wp_users as $wp_user) {
            $user_info = get_userdata($wp_user->ID);
            $user_roles = $user_info->roles;

            //Get the first user role if multiple roles exist
            if (is_array($user_roles)) {
                if (reset($user_roles)) {
                    $user_role = reset($user_roles);
                } else {
                    $user_role = 'none';
                }
            } else {
                $user_role = 'unknown';
            }
            ?>
                <tr>
                    <td><?= esc_html($user_info->user_login); ?></td>
                    <td><?= esc_html($user_info->display_name); ?></td>
                    <td><?= esc_html(ucfirst($user_role)); ?></td>
                </tr>
                <?php

            }
        }
    }

    function run_user_table()
    {
        $plugin = new User_Table();
    }

    run_user_table();