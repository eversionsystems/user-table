(function ($) {
    'use strict';

    var ajaxUrl = ut.ajax_url
    var wpRole = ''
    var sortOrder = 'ASC'
    var sortColumn = ''

    const paginationDiv = document.getElementById('pagination')
    const tableBody = document.getElementById('user_table_body')
    const wpRoleSelect = document.getElementById('wp_role_select')
    const ajaxSpinner = document.getElementById('ajax_spinner')

    $(function () {

        document.addEventListener('click', function (e) {
            if (e.target.matches('a.page-numbers')) {
                e.preventDefault()

                clickPagination(e)
            } else if (e.target.matches('a.sort-column')) {
                e.preventDefault()

                //Clear previous sorting
                document.querySelectorAll('.dashicons').forEach(e => e.parentNode.removeChild(e))

                clickSortColumn(e.target)
            }
        })

        wpRoleSelect.addEventListener('change', function (e) {
            //Clear previous sorting
            sortOrder = 'ASC'
            sortColumn = ''
            document.querySelectorAll('.dashicons').forEach(e => e.parentNode.removeChild(e))
            wpRole = wpRoleSelect.options[wpRoleSelect.selectedIndex].value

            clickFilterRoles()
        })

        function clickFilterRoles() {
            $.ajax({
                    url: ajaxUrl,
                    data: {
                        action: 'click_filter_roles',
                        wp_role: wpRole
                    },
                    type: 'POST',
                    beforeSend: function () {
                        showAjaxSpinner()
                    },
                }).done(function (data) {
                    paginationDiv.innerHTML = data.pagination
                    tableBody.innerHTML = data.user_rows
                })
                .fail(function () {
                    tableBody.innerHTML = 'Failed to retrive user rows.'
                    paginationDiv.innerHTML = ''
                }).always(function () {
                    ajaxSpinner.style.display = 'none'
                })
        }

        function clickSortColumn(column) {
            if ('ASC' === sortOrder) {
                sortOrder = 'DESC'
            } else {
                sortOrder = 'ASC'
            }

            if ('th_user_name' === column.id) {
                sortColumn = 'user_name'
            } else if ('th_display_name' === column.id) {
                sortColumn = 'display_name'
            }

            $.ajax({
                    url: ajaxUrl,
                    data: {
                        action: 'click_sort_column',
                        wp_role: wpRole,
                        sort_order: sortOrder,
                        sort_column: sortColumn
                    },
                    type: 'POST',
                    beforeSend: function () {
                        showAjaxSpinner()
                    },
                }).done(function (data) {
                    var spanClass = ''
                    paginationDiv.innerHTML = data.pagination
                    tableBody.innerHTML = data.user_rows

                    if ('DESC' === sortOrder) {
                        spanClass = 'dashicons dashicons-arrow-down'
                    } else {
                        spanClass = 'dashicons dashicons-arrow-up'
                    }

                    if (!column.firstElementChild) {
                        var node = document.createElement('SPAN')
                        node.className = spanClass
                        column.appendChild(node)
                    } else {
                        column.firstElementChild.className = spanClass
                    }
                })
                .fail(function () {
                    tableBody.innerHTML = 'Failed to sort columns.'
                    paginationDiv.innerHTML = ''
                }).always(function () {
                    ajaxSpinner.style.display = 'none'
                })
        }

        function clickPagination(e) {
            $.ajax({
                    url: ajaxUrl,
                    data: {
                        action: 'click_pagination',
                        pagination_url: e.target.href,
                        wp_role: wpRole,
                        sort_order: sortOrder,
                        sort_column: sortColumn
                    },
                    type: 'POST',
                    beforeSend: function () {
                        showAjaxSpinner()
                    },
                }).done(function (data) {
                    paginationDiv.innerHTML = data.pagination
                    tableBody.innerHTML = data.user_rows;
                })
                .fail(function () {
                    tableBody.innerHTML = 'Failed to retrieve user data.'
                    paginationDiv.innerHTML = ''
                }).always(function () {
                    ajaxSpinner.style.display = 'none'
                })
        }

        function showAjaxSpinner() {
            tableBody.innerHTML = ''
            paginationDiv.innerHTML = ''
            ajaxSpinner.style.display = 'block'
        }

    })

})(jQuery);